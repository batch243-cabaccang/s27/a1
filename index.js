// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Jane",
    "lastName": "DoeAgain",
    "email": "janedoeagain@mail.com",
    "password": "jane1234",
    "isAdmin": false,
    "mobileNo": "09123456789"
}



// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 22,
    "userId": 2,
    "productID" : 30,
    "transactionDate": "09-12-2021",
    "status": "paid",
    "total": 2100
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

{
    "id": 30,
    "name": "De-Humidifier",
    "description": "Make your home drier if you want.",
    "price": 700,
    "stocks": 203,
    "isActive": true,
}